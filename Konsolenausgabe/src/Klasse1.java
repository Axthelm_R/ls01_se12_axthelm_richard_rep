
public class Klasse1 {
	public static void main(String[] args) {
		
		//Aufgabe 1
		System.out.println("Aufgabe 1: ");
		System.out.println(" ");
		
		System.out.print("Dies ist der erste Satz. ");
		System.out.print("Und der zweite folgt sogleich.\n");
		System.out.println(" ");
			//Teil 2
		System.out.print("Dies ist der erste Satz.\n");
		System.out.print("Und der \"zweite\" folgt sogleich.");
		//print(); = es werden keine Zeilenumbrüche gemacht, println() = Es werden Zeilenumbrüche nach dem Text "" gesetzt
	
		System.out.println("  ");
		System.out.println("  ");
		System.out.println("Aufgabe 2: ");
		System.out.println(" ");
		//Aufgabe 2
		
		String no;
		no = "*";
		
		System.out.printf("%7s", no); //1
		System.out.printf("\n %7s \n",no + no + no); //2
		System.out.printf(" %8s \n",no + no + no + no + no); //3
		System.out.printf(" %9s \n",no+ no + no + no + no +no+no); //4
		System.out.printf(" %10s \n",no+ no + no + no + no+ no + no + no + no); //5
		System.out.printf(" %11s \n",no+ no + no + no + no+ no + no + no + no+no+no); //6
		System.out.println(no+ no + no + no + no+ no + no + no + no+no+no+no+no); //7
		System.out.printf(" %7s \n",no+no+no); //8
		System.out.printf(" %7s ",no+no+no); //9
		
		System.out.println(" ");
		System.out.println("Aufgabe 3: ");
		System.out.println(" ");
		
		//Aufgabe 3
		
		double zahl1, zahl2, zahl3, zahl4, zahl5;
		zahl1 = 22.4234234;
		zahl2 = 111.2222;
		zahl3 = 4.0;
		zahl4 = 1000000.551;
		zahl5 = 97.34;
		
		System.out.printf("%.2f \n",zahl1);
		System.out.printf("%.2f \n",zahl2);
		System.out.printf("%.2f \n",zahl3);
		System.out.printf("%.2f \n",zahl4);
		System.out.printf("%.2f \n",zahl5);
		 
	}
		
}

